from typing import Tuple

import bernard.model as model

STATUS = {
    10: "Jeune Espoir",
    50: "Accelère, Accelère !",
    100: "Tu es le Sang",
    150: "Tu boxes avec les mots",
    200: "Tu n'es qu'amour",
    300: "C'est la famille",
    500: "Célibatante",
    750: "La richesse te guette",
    1000: "Un vrai petit chat",
}


def get(username: str) -> model.User:
    """Return model.User."""
    return model.User.get(name=username)


def get_or_create(username: str) -> model.User:
    """Return model.User or create if doesn't exists."""
    user, created = model.User.get_or_create(name=username)
    user = user or created
    return user


def set_status(user: model.User) -> None:
    for messages, status in STATUS.items():
        if user.messages >= messages:
            user.status = status
    user.save()


def get_all_ranks() -> dict:
    """Retrieve classement.

    top return {0: (name:messages), 1: (name:messages),...}
    """
    # Get all users in database.
    users = model.User.select()

    # Create dict with username and messages then sort them.
    ranking = {
        user.name: user.messages for user in users if user.name != "bernard_caramel"
    }
    sorted_rank = sorted(ranking.items(), key=lambda kv: kv[1], reverse=True)
    # Add index + 1 to avoid rank 0.
    return {i + 1: x for i, x in enumerate(sorted_rank)}


def get_rank(username: str) -> Tuple[str, int]:
    user = get(username)
    for k, v in get_all_ranks().items():
        if user.name == v[0]:
            rank = k
            messages = v[1]
    return rank, messages


def increment(username: str) -> None:
    """Add one to user ranking and set status"""
    user = get_or_create(username)
    if user:
        user.messages += 1
        set_status(user)


def get_messages_and_status(username: str) -> Tuple[int, str]:
    user = get(username)
    return user.messages, user.status
