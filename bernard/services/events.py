import random
from typing import Optional

HELLO_MSG = (
    "Bonjour",
    "Bien le bonjour",
    "Hello",
    "Salut",
    "Yo",
    "Sympa de passer par là",
    "AH ! Bienvenu",
)

BLACKLIST = {
    "peskoooo",
    "peskoooo\r",
    "bernard_caramel",
    "bernard_caramel\r",
}


def ready(self):
    """Log when the bot is ready."""
    print(f"Ready | {self.nick}")


def message(message: str, author: str):
    """Log the received messages."""
    print(f"{author}: {message}")


def join(user: str) -> Optional[str]:
    return None if user in BLACKLIST else f"{random.choice(HELLO_MSG)} @{user} !"
