import twitchio.ext.commands as twitchio  # type: ignore

import bernard.model as model
import bernard.services.commands as commands
import bernard.services.events as events
import bernard.services.user as user
import bernard.tools.constants as constant


class Bot(twitchio.Bot):
    def __init__(self):
        super().__init__(
            client_id=constant.CLIENT_ID,
            initial_channels=[constant.CHANNEL],
            irc_token=constant.TMI_TOKEN,
            nick=constant.BOT_NICK,
            prefix="!",
        )

    async def event_ready(self):
        """Print a console message when bot is ready."""
        events.ready(self)

    async def event_message(self, message):
        """Handle message to redirect into commands function."""
        self.message = message.content
        self.author = message.author.name
        events.message(self.message, self.author)

        # Increment user whose are talking
        # without counting commands.
        if self.message[0] != "!":
            user.increment(self.author)

        # Handle message.
        await self.handle_commands(message)

    async def event_join(self, user):
        """Send a message to newest viewer."""
        message = events.join(user.name)
        if message:
            await user.channel.send(message)

    @twitchio.command(name="infos", aliases=["i", "info"])
    async def infos(self, ctx):
        """Command to show important links."""
        await ctx.send(commands.infos())

    @twitchio.command(name="chuck")
    async def chuck(self, ctx):
        """Command to show random sentence from Chuck."""
        await ctx.send(commands.chuck())

    @twitchio.command(name="velo", aliases=["bike"])
    async def velo(self, ctx):
        """Command to show information about next outdoor streaming."""
        await ctx.send(commands.velo())

    @twitchio.command(name="help", aliases=["h"])
    async def help(self, ctx):
        """Command to list available commands."""
        await ctx.send(commands.help())

    @twitchio.command(name="status")
    async def status(self, ctx):
        """Command to show user's status."""
        await ctx.send(commands.status(self.author))

    @twitchio.command(name="rank", aliases=["rang"])
    async def rank(self, ctx):
        """Command to show user's rank."""
        await ctx.send(commands.get_rank(self.author))

    @twitchio.command(name="top")
    async def top(self, ctx):
        """Command to show top 5 users."""
        await ctx.send(commands.get_top_5())


if __name__ == "__main__":
    model.user.init_database()
    bot = Bot()
    bot.run()
